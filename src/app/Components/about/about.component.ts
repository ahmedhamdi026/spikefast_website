import { ApiService } from 'src/app/Services/api.service';
import { Component, OnInit } from '@angular/core';
// import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
// import { NotifierService } from "angular-notifier"
import swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
// import { NgProgress, NgProgressRef } from '@ngx-progressbar/core';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {


  About: any[] | undefined;
  TeamMemmber: any[] | undefined;

  imageHost = environment.imageUrl;

  constructor(private ApiService: ApiService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.ApiService.GetAllAboutUs().subscribe(
      res => {
        this.About = res["data"];
      });

  }

}
