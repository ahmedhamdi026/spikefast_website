import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/Services/api.service';
import { environment } from 'src/environments/environment';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';



@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.css']
})
export class AnalysisComponent implements OnInit {

  ForexMarketAnalysis:any[]|undefined;
  CommodityAnalysis:any[]|undefined;
  CryptocurrencyAnalysis:any[]|undefined;

  Blogs:any[]|undefined;
  imageHost = environment.imageUrl;

  constructor(private apiservice:ApiService,public translate:TranslateService) { }

  ngOnInit(): void {

      // this.Blogs = this.route.snapshot.data['Blogs']
      this.apiservice.GetAllBlogsByStatue("1").subscribe(
         res => {
          this.ForexMarketAnalysis = res["data"];

      });

      this.apiservice.GetAllBlogsByStatue("2").subscribe(
        res => {
         this.CommodityAnalysis = res["data"];

     });

     this.apiservice.GetAllBlogsByStatue("3").subscribe(
      res => {
       this.CryptocurrencyAnalysis = res["data"];

   });

     


  }

}
