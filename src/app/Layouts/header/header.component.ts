import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  

  appearDash:any;
  enrolled:any;
  imageHost = environment.imageUrl;
  id:any;
  name:string;
  email:any;
  image:any; 
  constructor(private router:Router,public translate:TranslateService) 
  {
    this.name = localStorage.getItem('firstName') + " " +localStorage.getItem('lastName');
    this.id =  localStorage.getItem('id');
    this.email =  localStorage.getItem('email');
    this.image =  localStorage.getItem('image');
    this.appearDash =  localStorage.getItem('statue'+1);
    this.enrolled =  localStorage.getItem('token');

  }

  ngOnInit(): void {
    console.log(this.image);
  }



  logout(){
    
    localStorage.removeItem('statue');
    localStorage.removeItem('token');
    localStorage.clear();
    this.router.navigate(['/user/login']).then(() => {
      window.location.reload();
    });

  }

}
