import { ApiService } from 'src/app/Services/api.service';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  email:any;
  constructor(public translate:TranslateService, private apiservice:ApiService) { 
    this.email =  localStorage.getItem('email');
  }

  ngOnInit(): void {

  }

}
