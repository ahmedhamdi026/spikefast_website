import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

import { HttpClient, HttpHeaders , HttpParams } from '@angular/common/http'
import { Observable, of, } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { FormBuilder, FormGroup } from '@angular/forms';
// import { CookieService } from 'ngx-cookie-service';
import { User } from '../Models/User';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  endpoint = environment.apiHost;
  user_id: string|undefined;
  token: string|undefined;

  constructor(private http: HttpClient) {

   }

   httpOptions = {
    headers: new HttpHeaders({
      'content': 'application/json',
      'Authorization': 'Bearer'+' '+ localStorage.getItem("token"),
    })
  }

  
/*************************************books*******************************************/
 GetAllBooks(): Observable<any> {
  return this.http.get(this.endpoint +'/api/Books/GetAll', this.httpOptions)
}

/*************************************economic_calendar*******************************************/
GetAllCryptocurrency(): Observable<any> {
  return this.http.get(this.endpoint +'/api/economic_calendar/GetAll', this.httpOptions)
}

/*************************************forex_training*******************************************/
GetAllTrainingCalender(): Observable<any> {
  return this.http.get(this.endpoint +'/api/forex_training/GetAll', this.httpOptions)
}


/*************************************Terms*******************************************/
GetAllTerms(): Observable<any> {
  return this.http.get(this.endpoint +'/api/Terms/GetAll', this.httpOptions)
}

/*************************************About Us*******************************************/
 GetAllAboutUs(): Observable<any> {
  return this.http.get(this.endpoint +'/api/AboutUs/GetAll', this.httpOptions)
}

/*************************************Team Member*******************************************/
 GetAllTeamMembers(): Observable<any> {
  return this.http.get(this.endpoint +'/api/TeamMemmber/GetAll', this.httpOptions)
}


GetAll( Entity  :string  ): Observable<any> {
  return this.http.get(this.endpoint +'/api/'+ Entity +'/GetAll', this.httpOptions)
}
/*************************************Blogs*******************************************/
 GetAllBlogs(): Observable<any> {
  return this.http.get(this.endpoint +'/api/blog/GetAll',this.httpOptions)
}
GetAllBlogsByStatue(statue:any): Observable<any> {
  return this.http.get(this.endpoint +`/api/blog/GetAll_by_statue/${statue}`,this.httpOptions)
}

/*************************************Contact Us*******************************************/

 ContactUs(body:any): Observable<any> {
  return this.http.post(this.endpoint +'/api/ContactUs/Create',body,this.httpOptions)
}

/*************************************User*******************************************/

  CreateUser(body:User): Observable<any> {
    return this.http.post(this.endpoint +'/api/User/Register', body, this.httpOptions)
  }

  login(body: User): Observable<User> {
    return this.http.post<User>(this.endpoint + '/api/User/login', body, this.httpOptions)
  }

  
  loginAdmin(body:User): Observable<any> {

    return this.http.post(this.endpoint + '/api/User/loginAdmin', body, this.httpOptions)
  }

 
// **************************Upload Image***********************************************
  UploadsPost(selectedImage:any): Observable<any> {

    const formData = new FormData();
    formData.append("myFile", selectedImage);

    return this.http.post(this.endpoint + '/api/Uploads/Post', formData, this.httpOptions)
  }


}
