import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from 'src/app/Services/api.service';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/Models/User';

import { ToastrService } from 'src/app/Services/toastr.service';

@Component({
  selector: 'app-regestration',
  templateUrl: './regestration.component.html',
  styleUrls: ['./regestration.component.css']
})
export class RegestrationComponent implements OnInit {


  taskArr: any[] | undefined;
  taskArrD: any[] | undefined;
  mainid: any;
  taskid: any;
  imgSrc: string = "assets/images/image-placeholder.jpg";
  imgPath: string | undefined;
  p: number = 1;
  selectedImage: any = null;
  isSubmitted: boolean | undefined;
  taskArrlength: any;
  imageHost = environment.imageUrl;
 


  Userform = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    statue: new FormControl('',),
    image: new FormControl('',),

  })

  constructor(
    private ApiService: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr:ToastrService,
    
   
    ) {}

  ngOnInit(): void {}


  onSubmit(formValue: User) {

    if (this.Userform.valid) {
      let number: any = 1;
      formValue["statue"] = number
      formValue["image"] = this.imgPath
      this.ApiService.CreateUser(formValue).subscribe(
        res => { 

          this.toastr.success(res.email,"Thanks For Registration");
          // this._snackBar.open('Thank','For Registeration');
          this.router.navigate(['/user/login']);

        },error =>{
          this.toastr.error('Ops!',"Email is Already Register");

        });
    } else {
      this.toastr.warning('Ops!',"please Fill in the data correctly");
      // this._snackBar.open('Oops','please fill out all feildes correctly!');
    }

  }




  // resetForm() {
  //   this.Userform.reset();
  //   this.imgSrc = "assets/images/image-placeholder.jpg";
  //   this.selectedImage = null;
  //   this.isSubmitted = false;
  // }




  showPreview(event: any) {

    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.selectedImage = event.target.files[0];
      // this.progressRef.start();

      this.ApiService.UploadsPost(this.selectedImage).subscribe(
        res => {

          let filePath: any = res["filePath"];
          this.imgPath = filePath
          this.isSubmitted = true
          // this.progressRef.complete();

        });

    }
    else {
      this.imgSrc = "assets/images/image-placeholder.jpg";
      this.selectedImage = null;
    }
  }
}
